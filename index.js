const axios = require("axios");
var fs = require("fs");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
var mainhtml, tempaddr, tempname, mainurl;

function downloadentry(url, path = "./", name = "index") {
  mainurl = filefromurl(url).includes(".")
    ? url.slice(0, url.lastIndexOf("/") - 1)
    : url.replace(/\?.* /, "");
  tempaddr = path;
  tempname = name;
  if (!fs.existsSync(tempaddr)) {
    fs.mkdirSync(tempaddr);
  }
  if (fs.existsSync(tempaddr + tempname)) {
    console.error(" folder already exists");
    return;
  }
  fs.mkdirSync(tempaddr + tempname);
  fs.mkdirSync(tempaddr + tempname + "/js");
  fs.mkdirSync(tempaddr + tempname + "/css");
  fs.mkdirSync(tempaddr + tempname + "/css-assets");
  fs.mkdirSync(tempaddr + tempname + "/img");

  filedownloader(url, path + name + ".html").then(
    function(data) {
      parsehtml(path + name + ".html");
    },
    function(data) {
      console.error("can't download url : ", url);
    }
  );
}

function filedownloader(url, filename, prom = false) {
  var file = fs.createWriteStream(filename);
  return new Promise(function(res, rej) {
    axios({
      method: "get",
      url: url,
      responseType: "stream",
    })
      .then(function(resp) {
        if (resp.status && resp.status == 200) {
          resp.data.pipe(file).on("finish", function() {
            console.log("download completed : " + url);
            res();
          });
        } else {
          console.error("failed to download: " + url);
          rej();
        }
      })
      .catch(function(resp) {
        console.error("failed to download: " + url);
        rej();
      });
  });
}

function parsehtml(htmlfile) {
  fs.readFile(htmlfile, { encoding: "utf8" }, function(err, htmlstring) {
    if (err) {
      console.error(err);
    } else {
      const dom = new JSDOM(htmlstring);
      document = dom.window.document;

      var scripts = document.getElementsByTagName("script");
      for (i = 0; i < scripts.length; i++) {
        if (scripts[i].src) {
          let fn = filefromurl(scripts[i].src);
          filedownloader(scripts[i].src, tempaddr + tempname + "/js/" + fn);
          scripts[i].src = "/" + tempname + "/js/" + fn;
        }
      }

      var styles = document.querySelectorAll("link[rel='stylesheet']");
      for (i = 0; i < styles.length; i++) {
        let styleurl = styles[i].href;
        if (styleurl) {
          let fn = filefromurl(styleurl);
          let cssfilepath = tempaddr + tempname + "/css/" + fn;
          filedownloader(styleurl, cssfilepath).then(function() {
            fs.readFile(cssfilepath, { encoding: "utf8" }, function(
              err,
              csstext
            ) {
              fs.writeFile(
                cssfilepath,
                cssurlfixer(csstext, styleurl),
                function() {}
              );
            });
          });
          styles[i].href = "/" + tempname + "/css/" + fn;
        }
      }

      var inlinestyles = document.getElementsByTagName("style");
      for (i = 0; i < inlinestyles.length; i++) {
        inlinestyles[i].innerHTML = cssurlfixer(
          inlinestyles[i].innerHTML,
          mainurl
        );
      }
      var elementstyles = document.querySelectorAll("[style]");
      for (i = 0; i < elementstyles.length; i++) {
        elementstyles[i].attributes.style.textContent = cssurlfixer(
          elementstyles[i].attributes.style.textContent,
          mainurl
        );
      }

      var images = document.getElementsByTagName("img");
      for (i = 0; i < images.length; i++) {
        if (images[i].src) {
          let fn = filefromurl(images[i].src);
          filedownloader(images[i].src, tempaddr + tempname + "/img/" + fn);
          images[i].src = "/" + tempname + "/img/" + fn;
        }
      }

      var newhtml =
        "<!doctype html>\n" +
        document.getElementsByTagName("html")[0].outerHTML;
      fs.writeFile(htmlfile, newhtml, function() {});
    }
  });
}

function filefromurl(url) {
  let fn = url.slice(url.lastIndexOf("/") + 1);
  if (fn.indexOf("?") != -1) {
    fn = fn.slice(0, fn.indexOf("?"));
  }
  return fn;
}

function cssurlfixer(csstext, icururl) {
  return csstext.replace(/url.*?\((.+?)\)/g, function(
    match,
    contents,
    offset,
    input_string
  ) {
    let relativeurl = contents.replace(/'|"/g, "");
    let fn = filefromurl(relativeurl);
    let finalurl;
    if (
      relativeurl.slice(0, 4).toLowerCase() != "data" &&
      relativeurl.indexOf("/") != -1 &&
      relativeurl.slice(0, 4) != "http"
    ) {
      var cururl = icururl.split("/");
      cururl.pop();
      let addarray = relativeurl.split("/");
      if (addarray[0] == "") {
        finalurl = cururl[0] + relativeurl;
      }
      if (addarray[0] == ".") {
        finalurl = cururl.join("/") + "/" + addarray.shift().join("/");
      }
      while (addarray[0] == "..") {
        cururl.pop();
        addarray.shift();
      }
      if (!finalurl) {
        finalurl = cururl.join("/") + "/" + addarray.join("/");
      }
      filedownloader(finalurl, tempaddr + tempname + "/css-assets/" + fn);
      return `url("${tempname}/css-assets/${fn}")`;
    } else {
      return "url('" + contents + "')";
    }
  });
}

module.exports.downloader = downloadentry;
